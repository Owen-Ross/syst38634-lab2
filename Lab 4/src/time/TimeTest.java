package time;

import static org.junit.Assert.*;

import org.junit.Test;

public class TimeTest {


	@Test
	public void testGetTotalMilliseconds() {
		int totalMilliseconds = Time.getTotalMilliseconds("12:05:50:05");
		assertTrue("Invalid number of milliseconds", totalMilliseconds == 5);
	}
	
	@Test (expected=NumberFormatException.class)
	public void testGetTotalMillisecondsException() {
		int totalMilliseconds = Time.getTotalMilliseconds("12:05:50:0A");
		fail("Invalid number of milliseconds");
	}
	
	@Test
	public void testGetTotalMillisecondsBoundryIn() {
		int totalMilliseconds = Time.getTotalMilliseconds("12:05:50:999");
		assertTrue("Invalid number of milliseconds", totalMilliseconds == 999);
	}
	
	@Test (expected=NumberFormatException.class)
	public void testGetTotalMillisecondsBoundryOut() {
		int totalMilliseconds = Time.getTotalMilliseconds("12:05:50:1000");
		fail("Invalid number of milliseconds");
	}
	
	@Test
	public void testGetTotalSecondsRegular() {
		int totalSeconds = Time.getTotalSeconds("01:01:01");
		assertTrue("The Time provided does not match the result", totalSeconds == 3661);
	}

	@Test (expected=NumberFormatException.class)
	public void testGetTotalSecondsException() {
		int totalSeconds = Time.getTotalSeconds("01:01:0a");
		fail("The time provided is not valid");
	}
	
	@Test
	public void testGetTotalSecondsBoundryIn() {
		int totalSeconds = Time.getTotalSeconds("01:01:59");
		assertTrue("The Time provided does not match the result", totalSeconds == 3719);
	}
	
	@Test
	public void testGetTotalSecondsBoundryOut() {
		int totalSeconds = Time.getTotalSeconds("01:01:60");
		assertFalse("The time provided does not match the result", totalSeconds == 3721);
	}
	
}
